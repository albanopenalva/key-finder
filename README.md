# Implementación de Localizador de Llaves en EDU-CIAA NXP

![img1](https://bitbucket.org/albanopenalva/key-finder/raw/f41a9d252e9d965288abd5be543b9ea2ac9e8411/im1.png)

## Características

* Capacidad de adquisición de señal analógica con resolución de 10 bits y tasa de muestreo configurable.
* Procesamiento de la señal adquirida consistente en:
	1. Filtrado Pasa Altos (configurable)
    2. Rectificación
    3. Filtrado pasa Bajos para detección de envolvente (configurable)
    4. Umbralamiento (con niveles configurables)
* Reproducción analógica de la señal procesada, pudiendo seleccionarse cuales de las etapas de procesamiento mencionadas se le aplican a dicha señal.
* Disparo de alarma lumínica (parpadeo del led 1) al detectar un silbido.
* Detención de alarma lumínica (parpadeo del led 1) al detectar tres silbidos.

## Configuración

El firmware del localizador de llaves (KeyFinder) debe ser configurado antes de grabarse en la placa de desarrollo EDU-CIAA. 
Toda la estructura del firmware se encuentra contenida dentro de la carpeta "firmware", pero el código correspondiente a la aplicación en cuestión se encuentra en la ruta "firmware/projects/KeyFinder".
Para el correcto funcionamiento del sistema se debe:

1. Agregar en la carpeta "firmware/projects/KeyFinder/inc" los headers (.h) generados en MATLAB de los filtros Pasa Alto y Pasa Bajo (ver "Exportar filtros de MATLAB como Header en C.pdf").
2. Modificar el archivo "KF_config.h" que se encuentra en la carpeta "firmware/projects/KeyFinder/inc" para incluir los headers de los filtros y configurar el resto de los parámetros del sistema según su diseño.

![img2](https://bitbucket.org/albanopenalva/key-finder/raw/f41a9d252e9d965288abd5be543b9ea2ac9e8411/im2.png)

3. Grabar el firmware en la EDU-CIAA.

## Modo de Uso

#### Conexiones:

 * Ingreso de Señal de Audio: CH1
 * Salida de Señal Procesada: DAC
 * Tierra: GNDA
 
![img3](https://bitbucket.org/albanopenalva/key-finder/raw/f41a9d252e9d965288abd5be543b9ea2ac9e8411/im3.png)

#### Controles:
* Tecla 1: Cambia los modos de reproducción de la señal procesada, el modo seleccionado se indica mediante el led RGB:
	* Rojo: salida del filtro pasa altos
	* Verde: salida del rectificador
	* Azul: salida del detector de envolvente
	* Amarillo: salida del comparador
* Tecla 4: Inicia (led 3: On) y detiene (led 3: Off) el funcionamiento del sistema.

![img4](https://bitbucket.org/albanopenalva/key-finder/raw/f41a9d252e9d965288abd5be543b9ea2ac9e8411/im4.png)
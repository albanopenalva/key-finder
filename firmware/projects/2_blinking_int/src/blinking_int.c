/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "blinking_int.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "timer.h"

/*==================[macros and definitions]=================================*/
#define BIT0 0x01
#define TRUE 1

/*==================[internal data definition]===============================*/

/*==================[internal functions declaration]=========================*/


void time(void)
{
	static uint8_t mask;

	mask++;

	if((mask & BIT0) == TRUE)
	{
		LedOn(LED_1);
	}
	else
	{
		LedOff(LED_1);
	}
}

void time2(void)
{
	static uint8_t mask;

	mask++;

	if((mask & BIT0) == TRUE)
	{
		LedOn(LED_3);
	}
	else
	{
		LedOff(LED_3);
	}
}

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{
	SystemClockInit();
	LedsInit();
	SwitchesInit();
	timer_config timerA_setup = {TIMER_A,500,&time};
	TimerInit(&timerA_setup);

	timer_config timerB_setup = {TIMER_B,300,&time2};
	TimerInit(&timerB_setup);

	TimerStart(TIMER_A);
	TimerStart(TIMER_B);

    while(1);
    
}

/*==================[end of file]============================================*/


/*
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/KeyFinder.h"
#include "stdint.h"
#include "systemclock.h"
#include "timer.h"
#include "led.h"
#include "switch.h"
#include "analog_io.h"
#include "bool.h"
#include "iir.h"
#include "fpu_init.h"
#include "KF_config.h"
#include "math.h"

/*==================[macros and definitions]=================================*/

#define max(a, b) (((a) > (b)) ? (a) : (b))
#define abs(a) (((a) < (0)) ? (-a) : (a))

#define wL_HP max(NL_HP, DL_HP)
#define wL_LP max(NL_LP, DL_LP)

#define US 1000000			// micro segundos por segundo
#define VREF 3.3			// tensión de referencia del ADC (en V)
#define RES 10				// resolución del ADC en bits
#define T_SM 10 			// período en ms, entre toma de muestras de señal procesada para correr máq de estados
#define T_AL 30				// período en centésimas de seg para el parpadeo del led de alarma

typedef enum {ST_0, ST_1, ST_2, ST_3, ST_4, ST_5, ST_6  } state_t;
typedef enum {Off, On } stateal_t;
typedef enum {MOD_HP, MOD_RECT, MOD_ENV, MOD_COMP} mode_t;

// Inicialización de los parámetros configurados en K_config
#define TH_H U_H * pow(2, RES) / VREF	// umbral superior del comparador con histéresis
#define TH_L U_L * pow(2, RES) / VREF 	// umbral inferior del comparador con histéresis
#define TS US / FM						// período de muestreo en us
#define PULSE_HIGH_MAX SILB_H_MAX / 10	// duración máxima de un silbido (en centécimas de seg)
#define PULSE_HIGH_MIN SILB_H_MIN / 10	// duración mínima de un silbido (en centécimas de seg)
#define PULSE_LOW_MAX SILB_L_MAX / 10	// duración máxima entre silbidos (en centécimas de seg)
#define PULSE_LOW_MIN SILB_L_MIN / 10	// duración mínima entre silbidos (en centécimas de seg)

double w_HP[wL_HP];   	// vector de estados internos del filtro pasa altos
double w_LP[wL_LP];   	// vector de estados internos del filtro pasa bajos

/*==================[internal data definition]===============================*/

uint16_t input,output;

double inputD;				// señal de entrada
double outputD_hp;			// señal filtrada por el pasa alto
double outputD_rect;		// señal rectificada
double outputD_ed;			// señal de envolvente
uint16_t output_comp = 0;	// señal de salida del comparador

state_t st_now;			// estado del sistema
stateal_t stal_now;		// estado de la alarma
mode_t modo;			// modo de reproducción


/*==================[internal functions declaration]=========================*/

/* Función que se llama en cada interrupción del systick. Al ejecutarse adquiere
 * una nueva muestra, la procesa y reproduce por el DA la señal procesada
 * (según el modo elegido).
 */
void ProcesarMuestra(void) {
	static uint16_t threshold = TH_H;

	AnalogInputReadPolling(CH1, &input);
	inputD = (double) (input); // casteo a double

	// Filtro Pasa-Alto
	outputD_hp = iir(DL_HP - 1, DEN_HP, NL_HP - 1, NUM_HP, w_HP, inputD);

	// Detector de envolvente
	// Rectificacion
	outputD_rect = abs(outputD_hp);

	// Filtro Pasa-Bajo
	outputD_ed = iir(DL_LP - 1, DEN_LP, NL_LP - 1, NUM_LP, w_LP, outputD_rect);

	// Comparador con histeresis
	if (outputD_ed > threshold) {
		output_comp = 1000;
		threshold = TH_L;
	} else {
		output_comp = 0;
		threshold = TH_H;
	}

	// Se reproduce la muestra procesada en el DAC
	switch (modo) {
		case MOD_HP:
			AnalogOutputWrite((int16_t) outputD_hp + 512);
			break;
		case MOD_RECT:
			AnalogOutputWrite((uint16_t) outputD_rect);
			break;
		case MOD_ENV:
			AnalogOutputWrite((uint16_t) outputD_ed);
			break;
		case MOD_COMP:
			AnalogOutputWrite(output_comp);
			break;
	}

}

void MaquinaEstados(void){

	static uint16_t time_al = 0;	// Variable para conteo de tiempo alarma
	static uint16_t time = 0;		// Variable para conteo de duración de silbidos (en centésimas de seg)
	static uint8_t TA = 0, TB = 0;

	time++;

	// Maquina de estado para detección de silbidos
	switch (st_now) {
	case ST_0:
		if (output_comp > 0) {
			st_now = ST_1;
			time = 0;
		}
		break;
	case ST_1:
		if (output_comp > 0) {
			if (time > PULSE_HIGH_MIN) {
				st_now = ST_2;
			} else {
				st_now = ST_1;
			}
		} else {
			st_now = ST_0;
			TA = 0;
			TB = 0;
		}
		break;
	case ST_2:
		if (output_comp > 0) {
			if (time > PULSE_HIGH_MAX) {
				st_now = ST_6;
				TA = 0;
				TB = 0;
			}
		} else {
			if (time > PULSE_HIGH_MAX) {
				st_now = ST_0;
				TA = 0;
				TB = 0;
			} else {
				st_now = ST_3;
				TA += 1;
			}
		}
		break;
	case ST_3:
		if (output_comp == 0) {
			if (time > PULSE_LOW_MIN) {
				st_now = ST_4;
			} else {
				st_now = ST_0;
				TA = 0;
				TB = 0;
			}
		} else {
			st_now = ST_0;
			TA = 0;
			TB = 0;
		}
		break;
	case ST_4:
		if (output_comp == 0) {
			if (time >= PULSE_LOW_MAX) {
				st_now = ST_0;
				time = 0;
				TA = 0;
				TB = 0;
			} else {
				st_now = ST_4;
			}
		} else {
			if (time < PULSE_LOW_MAX) {
				st_now = ST_5;
				TB += 1;
			} else {
				st_now = ST_0;
				TA = 0;
				TB = 0;
			}
		}
		break;
	case ST_5:
		st_now = ST_0;
		break;
	case ST_6:
		if (output_comp > 0) {
			st_now = ST_6;
		} else {
			st_now = ST_0;
			TA = 0;
			TB = 0;
		}
		break;
	}

	// Máquina de estados de la Alarma
	switch (stal_now) {
	case On:
		time_al++;
		if(time_al > T_AL){
			LedToggle(LED_1);
			time_al = 0;
		}
		if ((TA == 3) && (TB == 2)) {
			TA = 0;
			TB = 0;
			st_now = ST_0;
			stal_now = Off;
		}
		break;
	case Off:
		LedOff(LED_1);
		if ((TA == 1) && (TB == 0)) {
			TA = 0;
			TB = 0;
			st_now = ST_0;
			stal_now = On;
		}
		break;
	}

}

void EncenderApagar(void){
	static bool encendido = false;

	encendido = !encendido;
	if (encendido){
		TimerReset(TIMER_A);
		TimerReset(TIMER_B);
		TimerStart(TIMER_A);
		TimerStart(TIMER_B);
		LedOn(LED_3);
	} else{
		TimerStop(TIMER_A);
		TimerStop(TIMER_B);
		LedOff(LED_3);
	}

}

/*
 * Función que se llama al presionar la tecla 4 y cambia la señal
 * que será reproducida en el DAC
 */
void CambiarModo(void){
	switch (modo) {
		case MOD_HP:
			modo = MOD_RECT;
			LedOff(LED_RGB_R);
			LedOn(LED_RGB_G);
			break;
		case MOD_RECT:
			modo = MOD_ENV;
			LedOff(LED_RGB_G);
			LedOn(LED_RGB_B);
			break;
		case MOD_ENV:
			modo = MOD_COMP;
			LedOff(LED_RGB_B);
			LedOn(LED_RGB_G);
			LedOn(LED_RGB_R);
			break;
		case MOD_COMP:
			modo = MOD_HP;
			LedOff(LED_RGB_G);
			LedOn(LED_RGB_R);
			break;
	}
}

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{
	SystemClockInit();
	fpuInit();

	// Configuración de ADC en canal 1
	analog_input_config adc = {
			CH1,
			AINPUTS_SINGLE_READ,
			NULL
	};
	AnalogInputInit(&adc);

	// Configuración del DAC
	AnalogOutputInit();

	// Configuración de Funciones de las Teclas
	SwitchesInit();
	SwitchActivInt(GPIOGP0, SWITCH_4, EncenderApagar);
	SwitchActivInt(GPIOGP1, SWITCH_1, CambiarModo);

	// Inicialiazción de Leds
	LedsInit();

	// Configuración de base de tiempo para correr máquina de estado
		timer_config timer_estados = {
				TIMER_B,
				T_SM,
				MaquinaEstados
		};
		TimerInit(&timer_estados);

	// Configuración de base de tiempo para el muestreo
	timer_config timer_muestreo = {
			TIMER_A,
			(uint16_t) (TS),
			ProcesarMuestra
	};
	TimerInit(&timer_muestreo);

	// Inicialización de estados
	st_now = ST_0;
	stal_now = Off;
	modo = MOD_HP;
	LedOn(LED_RGB_R);

	while(1)
	{
		
	}
    
	return 0;
}

/*==================[end of file]============================================*/


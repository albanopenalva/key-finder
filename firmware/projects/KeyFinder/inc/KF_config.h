/*
 * KF_config.h
 *
 *  Created on: 18 sep. 2019
 *      Author: AP
 */

#ifndef PROJECTS_KEYFINDER_INC_KF_CONFIG_H_
#define PROJECTS_KEYFINDER_INC_KF_CONFIG_H_

/*
 * Incluir Headers de los filtros diseñados en FDATool
 */
#include "pasaaltosIIR_8.h"
#include "pasabajosIIR_8.h"

/*
 * Definir parámetros del sistema
 */
#define FM 8000			// Frecuencia de muestreo (en Hz)

#define U_H 0.8			// Umbral alto del comparador con histéresis (en V)
#define U_L	0.5			// Umbral bajo del comparador con histéresis (en V)

#define SILB_H_MAX	800	// Duración máxima del silbido (en ms)
#define SILB_H_MIN	50	// Duración mínima del silbido (en ms)
#define SILB_L_MAX	800	// Duración máxima del espacio entre silbidos (en ms)
#define SILB_L_MIN	50	// Duración mínima del espacio entre silbidos (en ms)

#endif /* PROJECTS_KEYFINDER_INC_KF_CONFIG_H_ */
